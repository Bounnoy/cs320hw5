package com.cs320hw5;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class IfTest {

    @Test
    public void pp() {
        // Make console prints write to output stream so we can test display value.
        PrintStream stdout = System.out;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        String res = "if (2 < 10) then\n  n := 5\nelse\n  m := 7";
        Expr bin = new Bin('<', new Val(2), new Val(10));
        Stmt st1 = new Assign(new Var("n"), new Val(5));
        Stmt st2 = new Assign(new Var("m"), new Val(7));
        If test = new If(bin,st1,st2);
        test.pp();

        assertEquals(res,outContent.toString());
        System.setOut(stdout);
        System.out.println("\nIfTest pretty printing...");
        System.out.println("Expected: \n" + res);
        System.out.println("Actual: \n" + outContent.toString());
    }

    @Test
    public void exe() {
        Expr bin = new Bin('<', new Val(2), new Val(10));
        Var n = new Var("n");
        Var m = new Var("m");
        Stmt st1 = new Assign(n, new Val(5));
        Stmt st2 = new Assign(m, new Val(7));

        If test = new If(bin,st1,st2);
        test.exe();
        assertTrue(Expr.sym.containsKey(n.var)); // Conditional is true so "n" should be in symbol table.
        System.out.println("\nIfTest interpreting 'if (2 < 10) then n = 5 else m = 7'. (true if 'n = 5' is in symbol table)");
        System.out.println("Expected: true");
        System.out.println("Actual: " + Expr.sym.containsKey(n.var));

        bin = new Bin('>', new Val(2), new Val(10));
        test = new If(bin,st1,st2);
        test.exe();
        assertTrue(Expr.sym.containsKey(m.var)); // Conditional is false so "m" should be in symbol table.
        System.out.println("\nIfTest interpreting 'if (2 > 10) then n = 5 else m = 7'. (true if 'm = 7' is in symbol table)");
        System.out.println("Expected: true");
        System.out.println("Actual: " + Expr.sym.containsKey(m.var));
    }
}
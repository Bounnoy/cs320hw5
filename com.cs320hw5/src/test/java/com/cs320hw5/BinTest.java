package com.cs320hw5;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class BinTest {

    @Test
    public void eval() {

        // 10 + 5
        int res = 10 + 5;
        Expr bin = new Bin('+', new Val(10), new Val(5));
        assertEquals(res,bin.eval());
        System.out.println("\nBinTest evaluating '10 + 5'.");
        System.out.println("Expected: " + res);
        System.out.println("Actual: " + bin.eval());

        // 10 - 5
        res = 10 - 5;
        bin = new Bin('-', new Val(10), new Val(5));
        assertEquals(res,bin.eval());
        System.out.println("\nBinTest evaluating '10 - 5'.");
        System.out.println("Expected: " + res);
        System.out.println("Actual: " + bin.eval());

        // 10 * 5
        res = 10 * 5;
        bin = new Bin('*', new Val(10), new Val(5));
        assertEquals(res,bin.eval());
        System.out.println("\nBinTest evaluating '10 * 5'.");
        System.out.println("Expected: " + res);
        System.out.println("Actual: " + bin.eval());

        // 10 / 5
        res = 10 / 5;
        bin = new Bin('/', new Val(10), new Val(5));
        assertEquals(res,bin.eval());
        System.out.println("\nBinTest evaluating '10 / 5'.");
        System.out.println("Expected: " + res);
        System.out.println("Actual: " + bin.eval());

        boolean thrown = false;

        try {
            // 10 / 3
            bin = new Bin('/', new Val(10), new Val(3));
            bin.eval();
        }

        catch (ArithmeticException e) {
            thrown = true;
        }

        assertTrue(thrown);
        System.out.println("\nBinTest evaluating '10 / 3'. (true if exception thrown)");
        System.out.println("Expected: true");
        System.out.println("Actual: " + thrown);

        thrown = false;

        try {
            // 10 / 0
            bin = new Bin('/', new Val(10), new Val(0));
            bin.eval();
        }

        catch (ArithmeticException e) {
            thrown = true;
        }

        assertTrue(thrown);
        System.out.println("\nBinTest evaluating '10 / 0'. (true if exception thrown)");
        System.out.println("Expected: true");
        System.out.println("Actual: " + thrown);

        thrown = false;

        try {
            // 10 = 0
            bin = new Bin('=', new Val(10), new Val(0));
            bin.eval();
        }

        catch (IllegalArgumentException e) {
            thrown = true;
        }

        assertTrue(thrown);
        System.out.println("\nBinTest evaluating '10 = 0'. (invalid argument / true if exception thrown)");
        System.out.println("Expected: true");
        System.out.println("Actual: " + thrown);
    }

    @Test
    public void check() {
        Expr bin = new Bin('>', new Val(10), new Val(5));
        assertTrue(bin.check());
        System.out.println("\nBinTest checking '10 > 5'.");
        System.out.println("Expected: true");
        System.out.println("Actual: " + bin.check());

        bin = new Bin('<', new Val(10), new Val(5));
        assertFalse(bin.check());
        System.out.println("\nBinTest checking '10 < 5'.");
        System.out.println("Expected: false");
        System.out.println("Actual: " + bin.check());

        bin = new Bin('<', new Val(5), new Val(10));
        assertTrue(bin.check());
        System.out.println("\nBinTest checking '5 < 10'.");
        System.out.println("Expected: true");
        System.out.println("Actual: " + bin.check());

        bin = new Bin('>', new Val(5), new Val(10));
        assertFalse(bin.check());
        System.out.println("\nBinTest checking '5 > 10'.");
        System.out.println("Expected: false");
        System.out.println("Actual: " + bin.check());

        boolean thrown = false;

        try {
            // 10 = 0
            bin = new Bin('=', new Val(10), new Val(0));
            if (bin.check()) thrown = false;
        }

        catch (IllegalArgumentException e) {
            thrown = true;
        }

        assertTrue(thrown);
        System.out.println("\nBinTest checking '10 = 0'. (invalid argument / true if exception thrown)");
        System.out.println("Expected: true");
        System.out.println("Actual: " + thrown);
    }

    @Test
    public void show() {
        // Make console prints write to output stream so we can test display value.
        PrintStream stdout = System.out;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // 10 < 5
        String res = "(10 < 5)";
        Expr bin = new Bin('<', new Val(10), new Val(5));
        bin.show();
        assertEquals(res, outContent.toString());
        System.setOut(stdout);
        System.out.println("\nBinTest showing " + res);
        System.out.println("Expected: " + res);
        System.out.println("Actual: " + outContent.toString());
    }
}
package com.cs320hw5;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class WhileTest {

    @Test
    public void pp() {
        // Make console prints write to output stream so we can test display value.
        PrintStream stdout = System.out;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        String res = "while (n > 0) do\n  begin\n    r := (r * n)\n    n := (n - 1)\n  end";
        Var n = new Var("n");
        Var r = new Var("r");
        Expr five = new Val(5);
        Expr one = new Val(1);
        Expr zero = new Val(0);
        Stmt a = new Assign(n,five); // n = 5
        Stmt b = new Assign(r,one); // r = 1
        a.exe();
        b.exe();
        Expr bin = new Bin('>',n,zero); // (n > 0)
        Stmt [] st = new Stmt[2];
        st[0] = new Assign(r, new Bin('*', r, n));
        st[1] = new Assign(n, new Bin('-', n, one));
        Stmt comp = new Comp(st);
        Stmt w = new While(bin,comp);
        w.pp();
        assertEquals(res,outContent.toString());
        System.setOut(stdout);
        System.out.println("\nWhileTest pretty printing...");
        System.out.println("Expected: \n" + res);
        System.out.println("Actual: \n" + outContent.toString());
    }

    @Test
    public void exe() {
        int nn = 5;
        int rr = 1;
        while (nn > 0) {
            rr = (rr * nn);
            nn = (nn - 1);
        }

        Var n = new Var("n");
        Var r = new Var("r");
        Expr five = new Val(5);
        Expr one = new Val(1);
        Expr zero = new Val(0);
        Stmt a = new Assign(n,five); // n = 5
        Stmt b = new Assign(r,one); // r = 1
        a.exe();
        b.exe();
        Expr bin = new Bin('>',n,zero); // (n > 0)
        Stmt [] st = new Stmt[2];
        st[0] = new Assign(r, new Bin('*', r, n));
        st[1] = new Assign(n, new Bin('-', n, one));
        Stmt comp = new Comp(st);
        Stmt w = new While(bin,comp); // while (n > 0) do r = (r * n), n = (n - 1) end
        w.exe();

        assertEquals(rr,r.eval());
        System.out.println("\nWhileTest interpreting 'while (n > 0) do r = (r * n), n = (n - 1)' where n=5, r=1.");
        System.out.println("Expected: " + rr);
        System.out.println("Actual: " + r.eval());
    }
}
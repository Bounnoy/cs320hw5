package com.cs320hw5;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class OutputTest {

    @Test
    public void pp() {
        // Make console prints write to output stream so we can test display value.
        PrintStream stdout = System.out;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        String res = "output n";
        Var n = new Var("n");
        Expr ten = new Val(10);
        Stmt a = new Assign(n,ten);
        a.exe();
        Stmt o = new Output(n);
        o.pp();

        assertEquals(res,outContent.toString());
        System.setOut(stdout);
        System.out.println("\nOutputTest pretty printing...");
        System.out.println("Expected: " + res);
        System.out.println("Actual: " + outContent.toString());
    }

    @Test
    public void exe() {
        // Make console prints write to output stream so we can test display value.
        PrintStream stdout = System.out;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Let n = 0, m = 5, o = 20.
        // Multiply "m" and "o".
        // Add "n" to the multiple of "m" and "o".
        String res = "n = 100";
        Var n = new Var("n");
        Var m = new Var("m");
        Var o = new Var("o");
        Var p = new Var("p");
        Expr zero = new Val(0);
        Expr five = new Val(5);
        Expr twenty = new Val(20);

        Stmt [] st = new Stmt[5];
        st[0] = new Assign(n, zero); // n = 0
        st[1] = new Assign(m, five); // m = 5
        st[2] = new Assign(o, twenty); // o = 20
        st[3] = new Assign(p, new Bin('*',m,o));
        st[4] = new Assign(n, new Bin('+',n,p)); // n = n + (m * o)
        Stmt comp = new Comp(st);
        comp.exe();
        Stmt out = new Output(n);
        out.exe();

        assertEquals(res,outContent.toString());
        System.setOut(stdout);
        System.out.println("\nOutputTest interpreting 'n = 0, m = 5, o = 20, n + (m * o)'.");
        System.out.println("Expected: " + res);
        System.out.println("Actual: " + outContent.toString());
    }
}
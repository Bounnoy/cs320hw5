package com.cs320hw5;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class CompTest {

    @Test
    public void pp() {
        // Make console prints write to output stream so we can test display value.
        PrintStream stdout = System.out;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        String res = "begin\n  n := 10\n  m := 5\nend";
        Var n = new Var("n");
        Val ten = new Val(10);
        Stmt [] a = new Assign[2];
        a[0] = new Assign(n,ten);
        Var m = new Var("m");
        Val five = new Val(5);
        a[1] = new Assign(m,five);
        Stmt comp = new Comp(a);
        comp.pp();
        assertEquals(res,outContent.toString());
        System.setOut(stdout);
        System.out.println("\nCompTest pretty printing...");
        System.out.println("Expected: \n" + res);
        System.out.println("Actual: \n" + outContent.toString());
    }

    @Test
    public void exe() {
        // Test assign and if statement execution from inside compound statement.
        Var n = new Var("n");
        Var m = new Var("m");
        Var o = new Var("o");
        Expr ten = new Val(10);
        Expr five = new Val(5);
        Expr one = new Val(1);
        Bin bin = new Bin('<', one, five);
        Stmt [] join = new Stmt[2];
        join[0] = new Assign(n,ten);
        join[1] = new If(bin,new Assign(m,five),new Assign(o,one));
        Stmt com = new Comp(join);
        com.exe();

        // If successful...
        // "n" should have a value of 10.
        // "m" should have a value of 5.
        // "o" should not have a value.
        assertEquals(ten.eval(),n.eval());
        System.out.println("\nCompTest interpreting 'n = 10'.");
        System.out.println("Expected: " + ten.eval());
        System.out.println("Actual: " + n.eval());

        assertEquals(five.eval(),m.eval());
        System.out.println("\nCompTest interpreting 'if (m = 5) then o = 1'.");
        System.out.println("Expected: " + five.eval());
        System.out.println("Actual: " + m.eval());

        boolean thrown = false;

        try {
            int ex = o.eval();
        }

        catch (NullPointerException e) {
            thrown = true;
        }

        assertTrue(thrown);
        System.out.println("\nCompTest interpreting 'if (m = 5) then o = 1'. (true if exception thrown)");
        System.out.println("Expected: true");
        System.out.println("Actual: " + thrown);
    }
}
package com.cs320hw5;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class VarTest {

    @Test
    public void eval() {
        Val res = new Val(10);
        Var var = new Var("n");
        var.set(new Val(10));
        assertEquals(res.eval(),var.eval());
        System.out.println("\nVarTest evaluating 'n = 10'.");
        System.out.println("Expected: " + res.eval());
        System.out.println("Actual: " + var.eval());
    }

    @Test
    public void show() {
        // Make console prints write to output stream so we can test display value.
        PrintStream stdout = System.out;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        String res = "n";
        Var var = new Var("n");
        var.show();
        assertEquals(res, outContent.toString());
        System.setOut(stdout);
        System.out.println("\nVarTest showing variable 'n'.");
        System.out.println("Expected: " + res);
        System.out.println("Actual: " + outContent.toString());
    }

    @Test
    public void set() {
        Expr res = new Val(10);
        Var var = new Var("n");
        var.set(res);
        assertTrue(Expr.sym.containsKey(var.var));
        System.out.println("\nVarTest setting variable 'n' to '10'. (true if 'n' is in symbol table)");
        System.out.println("Expected: true");
        System.out.println("Actual: " + Expr.sym.containsKey(var.var));
    }
}
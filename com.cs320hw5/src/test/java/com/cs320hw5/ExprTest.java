package com.cs320hw5;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class ExprTest {

    @Test
    public void eval() {

        // (1 + 2) * 3
        Expr exp = new Bin('*', new Bin('+', new Val(1), new Val(2)), new Val(3));
        assertEquals(9, exp.eval());
        System.out.println("\nExprTest evaluating '(1 + 2) * 3'.");
        System.out.println("Expected: 9");
        System.out.println("Actual: " + exp.eval());

        // 4 / (3 - 1) DIVISION OK
        exp = new Bin('/', new Val(4), new Bin('-', new Val(3), new Val(1)));
        assertEquals(2, exp.eval());
        System.out.println("\nExprTest evaluating '4 / (3 - 1)'.");
        System.out.println("Expected: 2");
        System.out.println("Actual: " + exp.eval());

        boolean thrown = false;

        try {
            // 4 / (1 * 3) DIVISION NOT OK
            exp = new Bin('/', new Val(4), new Bin('*', new Val(1), new Val(3)));
            exp.eval();
        }

        catch (ArithmeticException e) {
            thrown = true;
        }

        assertTrue(thrown);
        System.out.println("\nExprTest evaluating '4 / (1 * 3)'. (true if exception thrown)");
        System.out.println("Expected: true");
        System.out.println("Actual: " + thrown);

    }

    @Test
    public void show() {

        // Make console prints write to output stream so we can test display value.
        PrintStream stdout = System.out;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // (1 + 2) * 3
        String res = "((1 + 2) * 3)";
        Expr exp = new Bin('*', new Bin('+', new Val(1), new Val(2)), new Val(3));
        exp.show();
        assertEquals(res, outContent.toString());
        System.setOut(stdout);
        System.out.println("\nExprTest showing expression...");
        System.out.println("Expected: " + res);
        System.out.println("Actual: " + outContent.toString());

        System.setOut(new PrintStream(outContent));
        outContent.reset();

        // 4 / (3 - 1) DIVISION OK
        res = "(4 / (3 - 1))";
        exp = new Bin('/', new Val(4), new Bin('-', new Val(3), new Val(1)));
        exp.show();
        assertEquals(res, outContent.toString());
        System.setOut(stdout);
        System.out.println("\nExprTest showing expression...");
        System.out.println("Expected: " + res);
        System.out.println("Actual: " + outContent.toString());

        System.setOut(new PrintStream(outContent));
        outContent.reset();

        // 4 / (1 * 3) DIVISION NOT OK
        res = "(4 / (1 * 3))";
        exp = new Bin('/', new Val(4), new Bin('*', new Val(1), new Val(3)));
        exp.show();
        assertEquals(res, outContent.toString());
        System.setOut(stdout);
        System.out.println("\nExprTest showing expression...");
        System.out.println("Expected: " + res);
        System.out.println("Actual: " + outContent.toString());
    }
}
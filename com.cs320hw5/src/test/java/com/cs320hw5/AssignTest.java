package com.cs320hw5;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class AssignTest {

    @Test
    public void pp() {
        // Make console prints write to output stream so we can test display value.
        PrintStream stdout = System.out;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        String res = "n := 10";
        Var var = new Var("n");
        Expr val = new Val(10);
        Stmt st = new Assign(var,val);
        st.pp();

        assertEquals(res,outContent.toString());
        System.setOut(stdout);
        System.out.println("\nAssignTest pretty printing 'n = 10'.");
        System.out.println("Expected: " + res);
        System.out.println("Actual: " + outContent.toString());
    }

    @Test
    public void exe() {
        Var var = new Var("n");
        Expr val = new Val(10);
        Stmt st = new Assign(var,val);
        st.exe();

        assertTrue(Expr.sym.containsKey(var.var));
        System.out.println("\nAssignTest checking if variable 'n' is in symbol table.");
        System.out.println("Expected: true");
        System.out.println("Actual: " + Expr.sym.containsKey(var.var));

        assertTrue(Expr.sym.containsValue(val.eval()));
        System.out.println("\nAssignTest checking if value of '10' is in symbol table.");
        System.out.println("Expected: true");
        System.out.println("Actual: " + Expr.sym.containsValue(val.eval()));
    }
}
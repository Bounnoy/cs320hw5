package com.cs320hw5;

public class While extends Stmt {
    protected Expr exp;
    protected Stmt st;

    public While(Expr exp, Stmt st) {
        this.exp = exp;
        this.st = st;
    }

    public void pp() {
        indent();

        System.out.print("while ");
        exp.show();
        System.out.println(" do");

        ++inSize;
        st.pp();
        --inSize;
    }

    public void exe() {
        if (exp.check()) {
            st.exe();
            exe();
        }
    }
}

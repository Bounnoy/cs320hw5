package com.cs320hw5;

abstract class Stmt {

    public static int inSize;
    public static void indent() {
        for (int i = 0; i < inSize; i++)
            System.out.print("  ");
    }

    abstract public void pp();
    abstract public void exe();

}

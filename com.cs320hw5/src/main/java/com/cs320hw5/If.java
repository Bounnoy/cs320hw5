package com.cs320hw5;

public class If extends Stmt {
    protected Expr exp;
    protected Stmt st1;
    protected Stmt st2;

    public If(Expr exp, Stmt st1, Stmt st2) {
        this.exp = exp;
        this.st1 = st1;
        this.st2 = st2;
        exe();
    }

    public void pp() {
        indent();
        System.out.print("if ");
        exp.show();
        System.out.print(" then\n");

        ++inSize;
        st1.pp();
        --inSize;

        indent();
        System.out.println("\nelse");

        ++inSize;
        st2.pp();
        --inSize;
    }

    public void exe() {
        if (exp.check()) st1.exe();
        else st2.exe();
    }
}

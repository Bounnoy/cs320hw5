package com.cs320hw5;

import java.util.NoSuchElementException;

public class Var extends Expr {
    protected String var;

    public Var(String var) {
        this.var = var;
    }

    public int eval() throws NullPointerException {
        return (int) sym.get(this.var);
    }

    public void show() {
        System.out.print(var);
    }

    public void set(Expr val) {
        sym.put(this.var,val.eval());
    }

    public boolean check() {
        if (var != null && Expr.sym.containsKey(var))
            return true;
        return false;
    }

}

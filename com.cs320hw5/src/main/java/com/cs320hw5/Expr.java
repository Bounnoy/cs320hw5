package com.cs320hw5;

import java.util.HashMap;
import java.util.Map;

abstract class Expr {
    public static Map sym = new HashMap();
    abstract public int eval();
    abstract public void show();
    abstract public boolean check();

}

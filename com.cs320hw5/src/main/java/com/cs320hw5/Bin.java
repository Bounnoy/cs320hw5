package com.cs320hw5;

public class Bin extends Expr {
    protected Expr left;
    protected char middle;
    protected Expr right;

    public Bin(char middle, Expr left, Expr right) {
        this.left = left;
        this.middle = middle;
        this.right = right;
    }

    public int eval() throws ArithmeticException, IllegalArgumentException {
        int res;
        switch (middle) {

            case '+':
                res = left.eval() + right.eval();
                break;

            case '-':
                res = left.eval() - right.eval();
                break;

            case '*':
                res = left.eval() * right.eval();
                break;

            case '/':
                if (left.eval() % right.eval() != 0)
                    throw new ArithmeticException("error");

                res = left.eval() / right.eval();
                break;

            case '%':
                res = left.eval() % right.eval();
                break;

            default:
                throw new IllegalArgumentException("error");
        }

        return res;
    }

    public boolean check() throws IllegalArgumentException {
        boolean res = false;
        switch (middle) {
            case '<':
                if (left.eval() < right.eval()) res = true;
                break;
            case '>':
                if (left.eval() > right.eval()) res = true;
                break;
            default:
                throw new IllegalArgumentException("error");
        }

        return res;
    }

    public void show() {
        System.out.print("(");
        left.show();
        System.out.print(" " + middle + " ");
        right.show();
        System.out.print(")");
    }
}

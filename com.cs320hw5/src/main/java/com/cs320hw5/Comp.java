package com.cs320hw5;

public class Comp extends Stmt {
    protected Stmt [] st;

    public Comp(Stmt [] st) {
        int l = st.length;
        this.st = new Stmt[l];
        for (int i = 0; i < l; i++)
            this.st[i] = st[i];
    }

    public void pp() {
        indent();
        System.out.println("begin");

        ++inSize;
        for (Stmt x : st) {
            x.pp();
            System.out.println();
        }
        --inSize;

        indent();
        System.out.print("end");
    }

    public void exe() {

        for (Stmt x : st)
            x.exe();
    }
}

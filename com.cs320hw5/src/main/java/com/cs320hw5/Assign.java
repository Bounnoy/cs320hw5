package com.cs320hw5;

public class Assign extends Stmt {
    protected Var var;
    protected Expr exp;

    public Assign(Var var, Expr exp) {
        this.var = var;
        this.exp = exp;
    }

    public void pp() {
        indent();
        var.show();
        System.out.print(" := ");
        exp.show();
    }

    public void exe() {
        var.set(exp);
    }
}

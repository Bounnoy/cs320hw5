package com.cs320hw5;

public class Main {

    public static void main(String[] args) {

        try {
            // Factorial program from assignment instructions.
            System.out.println("------------------------------\n" +
                    "        Factorial(5)\n" +
                    "------------------------------\n");
            Stmt factorial = new Comp(new Stmt[]{
                    new Assign(new Var("n"), new Val(5)),
                    new Assign(new Var("r"), new Val(1)),
                    new While(new Bin('>', new Var("n"), new Val(0)),
                            new Comp(new Stmt[]{
                                    new Assign(new Var("r"), new Bin('*', new Var("r"), new Var("n"))),
                                    new Assign(new Var("n"), new Bin('-', new Var("n"), new Val(1)))
                            })),
                    new Output(new Var("r"))
            });

            factorial.pp();
            System.out.println("\n\n---------- Results -----------\n");
            factorial.exe();
        }

        catch (ArithmeticException | IllegalArgumentException | NullPointerException e) {
            System.out.println("error");
        }

        try {
            // My program of choice.
            // Least common multiple of 16 and 36.
            // LCM formula: (a * b) / gcd(a, b)
            System.out.println("\n\n------------------------------\n" +
                    " Least Common Multiple (16,36)\n" +
                    "------------------------------\n" +
                    "LCM Formula: (a * b) / gcd(a, b)\n" +
                    "a1 and b1 below calculates gcd\n" +
                    "a2 and b2 below calculates lcm\n");

            Stmt lcd = new Comp(new Stmt[]{
                    new Assign(new Var("a1"), new Val(16)), // a1 = 16
                    new Assign(new Var("b1"), new Val(36)), // b1 = 36
                    new Assign(new Var("a2"), new Val(16)), // a2 = 16 (extra copy of "a" for calculations)
                    new Assign(new Var("b2"), new Val(36)), // b2 = 36
                    new Assign(new Var("r"), new Val(0)),  // r = 0   <-- Remainder
                    new While(new Bin('>', new Var("b1"), new Val(0)), // Need to find gcd first.
                            new Comp(new Stmt[]{
                                    new Assign(new Var("r"), new Bin('%', new Var("a1"), new Var("b1"))),
                                    new Assign(new Var("a1"), new Var("b1")),
                                    new Assign(new Var("b1"), new Var("r"))
                            })), // a1 is the result of gcd(a, b)
                    new Assign(new Var("a2"), new Bin('/', new Bin('*', new Var("a2"), new Var("b2")), new Var("a1"))),
                    new Output(new Var("a2")) // r is now the result of lcm
            });

            lcd.pp();
            System.out.println("\n\n---------- Results -----------\n");
            lcd.exe();
            System.out.println("\n\n------------------------------\n");
        }

        catch (ArithmeticException | IllegalArgumentException | NullPointerException e) {
            System.out.println("error");
        }
    }
}

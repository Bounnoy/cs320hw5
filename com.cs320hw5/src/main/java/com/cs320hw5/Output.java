package com.cs320hw5;

public class Output extends Stmt {
    protected Var var;

    public Output(Var var) {
        this.var = var;
    }

    public void pp() {
        indent();
        System.out.print("output ");
        var.show();
    }

    public void exe() {
        var.show();
        System.out.print(" = " + var.eval());
    }
}

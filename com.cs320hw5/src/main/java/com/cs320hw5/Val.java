package com.cs320hw5;

public class Val extends Expr {

    protected int value;

    public Val(int value) {
        this.value = value;
    }

    public int eval() {
        return value;
    }

    public void show() {
        System.out.print(value);
    }

    public boolean check() {
        return true;
    }
}
